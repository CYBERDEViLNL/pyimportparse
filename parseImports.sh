#!/bin/bash
SEARCH_PATH=$1;

if [ ! -d "$SEARCH_PATH" ]; then
	echo "Please give a valid path as first argument.";
	echo "example: parseImports.sh /home/foo/code/myProject/";
	exit;
fi

IMPORTS=($(grep -rwhoP --include \*.py $SEARCH_PATH -e "^import (\w+\.?)+" | sort -u));
FROM_IMPORTS=($(grep -rwhoP --include \*.py $SEARCH_PATH -e "^from \K(\w+\.?)+ import (\w+\.?)+" | sed "s/\b import \b/\./g" | sort -u));

EXTERNAL_IMPORTS=();
LOCAL_IMPORTS=();
process () {
	SUB_PATHS=($(IFS='.';echo $1));
	TEST_PATH="."
	ROOT_LOCAL=0;
	INDEX=$((${#SUB_PATHS[*]} - 1));
	CLASS_NAME="${SUB_PATHS[$INDEX]}";

	for SUB_PATH in "${SUB_PATHS[@]}"; do
		if [ "$CLASS_NAME" != "$SUB_PATH" ]; then
			TEST_PATH="$TEST_PATH/$SUB_PATH";
		fi
	done

	if [ -f "./${SUB_PATHS[0]}/__init__.py" ]; then
		ROOT_LOCAL=1;
		if [[ "${LOCAL_IMPORTS[*]}" != *"$1"* ]]; then
			LOCAL_IMPORTS+=($1);
		fi
	else
		if [[ "${EXTERNAL_IMPORTS[*]}" != *"$1"* ]]; then
			EXTERNAL_IMPORTS+=($1);
		fi
	fi
}

for FROM_IMPORT in "${FROM_IMPORTS[@]}"; do
	process "$FROM_IMPORT";
done;
for IMPORT in "${IMPORTS[@]}"; do
	process "$IMPORT" 1;
done;

IFS=$'\n' LOCAL_IMPORTS_S=($(sort <<<"${LOCAL_IMPORTS[*]}"))
for LOCAL_IMPORT in "${LOCAL_IMPORTS_S[@]}"; do
	echo -e "LOCAL IMPORT:\t\t$LOCAL_IMPORT";
done;

IFS=$'\n' EXTERNAL_IMPORTS_S=($(sort <<<"${EXTERNAL_IMPORTS[*]}"))
for EXTERNAL_IMPORT in "${EXTERNAL_IMPORTS_S[@]}"; do
	echo -e "EXTERNAL IMPORT:\t$EXTERNAL_IMPORT";
done;

unset IFS
