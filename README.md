# pyImportParse
Simple bash script which prints all imports for a given Python project directory. It will recursively scan for `.py` files.

# Usage
```bash
./parseImports.sh /path/to/your/pyProject/
```

### Output example:
```bash
LOCAL IMPORT:		core.cache.Cache
LOCAL IMPORT:		core.client.RecordClient
LOCAL IMPORT:		core.client.VideoStatus
LOCAL IMPORT:		core.jsParse.JsParse
LOCAL IMPORT:		core.packets.Packets
LOCAL IMPORT:		core.plugin.ResolverPluginProto
LOCAL IMPORT:		core.plugin.SeleniumPluginProto
LOCAL IMPORT:		core.postProc.PostProcThread
LOCAL IMPORT:		core.server.RecordThread
LOCAL IMPORT:		core.status.ItemStatus
LOCAL IMPORT:		models.cue.CueModel
LOCAL IMPORT:		models.settings.Settings
LOCAL IMPORT:		plugins.npo.metadata.NpoPlugin
LOCAL IMPORT:		plugins.npoStart.metadata.NpoStartPlugin
LOCAL IMPORT:		utils.pulseAudio.PulseHandle
LOCAL IMPORT:		views.cue.CueView
LOCAL IMPORT:		views.settings.SettingsView
LOCAL IMPORT:		widgets.group.GroupView
LOCAL IMPORT:		widgets.hPair.HPair
LOCAL IMPORT:		widgets.tools.ItemEdit
EXTERNAL IMPORT:	PyQt5.QtCore.QObject
EXTERNAL IMPORT:	PyQt5.QtCore.QThread
EXTERNAL IMPORT:	PyQt5.QtCore.Qt
EXTERNAL IMPORT:	PyQt5.QtGui.QIntValidator
EXTERNAL IMPORT:	PyQt5.QtWidgets.QAction
EXTERNAL IMPORT:	PyQt5.QtWidgets.QApplication
EXTERNAL IMPORT:	PyQt5.QtWidgets.QHBoxLayout
EXTERNAL IMPORT:	PyQt5.QtWidgets.QWidget
EXTERNAL IMPORT:	argparse
EXTERNAL IMPORT:	bs4.BeautifulSoup
EXTERNAL IMPORT:	configparser
EXTERNAL IMPORT:	datetime.datetime
EXTERNAL IMPORT:	http.cookiejar
EXTERNAL IMPORT:	http.server.HTTPServer
EXTERNAL IMPORT:	importlib.import_module
EXTERNAL IMPORT:	json
EXTERNAL IMPORT:	locale
EXTERNAL IMPORT:	os
EXTERNAL IMPORT:	requests.exceptions.ConnectionError
EXTERNAL IMPORT:	selenium.common.exceptions.ElementNotInteractableException
EXTERNAL IMPORT:	selenium.common.exceptions.NoSuchElementException
EXTERNAL IMPORT:	selenium.common.exceptions.StaleElementReferenceException
EXTERNAL IMPORT:	selenium.common.exceptions.TimeoutException
EXTERNAL IMPORT:	selenium.webdriver
EXTERNAL IMPORT:	selenium.webdriver.common.by.By
EXTERNAL IMPORT:	selenium.webdriver.support.expected_conditions
EXTERNAL IMPORT:	selenium.webdriver.support.ui.WebDriverWait
EXTERNAL IMPORT:	signal
EXTERNAL IMPORT:	socket
EXTERNAL IMPORT:	struct
EXTERNAL IMPORT:	subprocess
EXTERNAL IMPORT:	sys
EXTERNAL IMPORT:	urllib.error
EXTERNAL IMPORT:	urllib.parse.parse_qs
EXTERNAL IMPORT:	urllib.parse.urlparse
EXTERNAL IMPORT:	urllib.request
```
